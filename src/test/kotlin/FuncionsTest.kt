import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class FuncionsTest{

    @Test
    fun getOccurrencesNone(){
        val funs = Funcions()
        assertEquals(0, funs.getOccurrences("aaa", 'b'))
    }

    @Test
    fun getOccurrencesMoreThanOne(){
        val funs = Funcions()
        assertEquals(3, funs.getOccurrences("paraula", 'a'))
    }

    @Test
    fun getOccurrencesStarting(){
        val funs = Funcions()
        assertEquals(1, funs.getOccurrences("abc", 'a'))
    }

    @Test
    fun getOccurrencesEnding(){
        val funs = Funcions()
        assertEquals(1, funs.getOccurrences("abc", 'c'))
    }
}