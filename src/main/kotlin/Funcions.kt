import java.util.*

class Funcions {


    /**
     * Retorna un valor positiu llegit del Scanner passat per parametre
     *
     * @param sc Scanner d'on es llegeixen els enters
     * @return valor enter positiu
     */
    fun getPositiveIntFromScanner(sc: Scanner): Int {
        //TODO: can it use a do-while? Make it
        var value: Int = -1
        while (value < 0) {
             value = sc.nextInt()
        }
        return value
    }


    fun getOccurrences(abc: String, b: Char): Int {
        var coincidencies = 0
        for (char in abc) {
            if (char == b) {
                coincidencies++
            }
        }
        return coincidencies
    }


}